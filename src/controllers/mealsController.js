const fs = require('fs');
const path = require('path');

const duongdan = path.resolve(__dirname, '../../dist/TTNT_2/seg_train/bongda');
const destduongdan = path.resolve(__dirname, '../../dist/TTNT_3/seg_train');

exports.getMenuController = async (req, res, next) => {

  if ((req.query.category) && (req.query.filename) && (req.query.level)) {
    if (req.query.level == "positive") {
      fs.copyFile(`${duongdan}/${req.query.filename}`, `${destduongdan}/soccer_posi/${req.query.filename}`, (err => {
        if (err) console.log("copy loi", err);
        else fs.unlink(`${duongdan}/${req.query.filename}`, err => {
          if (err) console.log("xoa loi", err);
          else hienthi(res);
        });
      }));
    }
    if (req.query.level == "neutral") {
      fs.copyFile(`${duongdan}/${req.query.filename}`, `${destduongdan}/soccer_neut/${req.query.filename}`, (err => {
        if (err) console.log("copy loi", err);
        else fs.unlink(`${duongdan}/${req.query.filename}`, err => {
          if (err) console.log("xoa loi", err);
          else hienthi(res);
        });
      }));
    }
    if (req.query.level == "negative") {
      fs.copyFile(`${duongdan}/${req.query.filename}`, `${destduongdan}/soccer_nega/${req.query.filename}`, (err => {
        if (err) console.log("copy loi", err);
        else fs.unlink(`${duongdan}/${req.query.filename}`, err => {
          if (err) console.log("xoa loi", err);
          else hienthi(res);
        });
      }));
    }
    if (req.query.level == "delete") {
      fs.unlink(`${duongdan}/${req.query.filename}`, err => {
        if (err) console.log("xoa loi", err);
        else hienthi(res);
      });
    }
  }
  else {
    hienthi(res);
  }
  
  
};

function hienthi(res) {
  fs.readdir(duongdan, (err, files) => {
    if (err) console.log(err);
    else {
      console.log(files.length)
      if (files.length > 0) {
        const namefile = files[0];
        const category = "bongda";
        const dir = "/TTNT_2/seg_train/bongda";
        console.log(encodeURI(namefile));
        res.render("menu", {namefile, category , dir});
      }
      else res.render("index");
    }
  });
}
